//
//  CellCounterTableViewCell.swift
//  Counter NC-2
//
//  Created by Ekaterina Musiyko on 12/12/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import UIKit

class CellCounterTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBOutlet weak var counter: UILabel!
    @IBOutlet weak var minus: UIButton!
    

    var n = 0 {
        didSet {
            counter.text = String (n)
        }
    }
    
    
    @IBAction func minus (sender: UIButton){
        n -= 1
    }
    
    @IBAction func plus (sender: UIButton){
        n += 1
    }
    
    func resetTheCell(){
        n=0
    }
    
}

