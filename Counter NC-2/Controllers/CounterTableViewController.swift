//
//  CounterTableViewController.swift
//  Counter NC-2
//
//  Created by Ekaterina Musiyko on 12/12/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import UIKit

class CounterTableViewController: UITableViewController {
    
var numberOfRows = [0,1,2]
    var cells: [CellCounterTableViewCell] = []
    //, CellCounterTableViewCell(), CellCounterTableViewCell()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

       
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return numberOfRows.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCounter", for: indexPath) as! CellCounterTableViewCell
        
        cells.append(cell)
        
       // cell.counter.text = String (cell.n)
        let backGrounds = [ #colorLiteral(red: 0.8274509804, green: 0.9568627451, blue: 1, alpha: 1), #colorLiteral(red: 1, green: 0.9921568627, blue: 0.9764705882, alpha: 1), #colorLiteral(red: 1, green: 0.8901960784, blue: 0.9294117647, alpha: 0.8470588235) ]
        cell.backgroundColor = backGrounds [indexPath.row]
        
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
       /* if editingStyle == .delete {
            numberOfRows.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            // handle delete (by removing the data from your array and updating the tableview)
        } else if editingStyle == .insert {
        
        let reset = UIContextualAction(style: .normal, title: "Reset") { _, _, _ in
          print("ok")
        }
        reset.backgroundColor = .green
        } */
    }
    
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
  //  let cell = tableView.dequeueReusableCell(withIdentifier: "cellCounter", for: indexPath) as! CellCounterTableViewCell
        let cell = cells[indexPath.row]
      let reset = UIContextualAction(style: .normal, title: "Reset") { _, _, _ in
       // tableView.cell.counter.text = "Restart"
      //  tableView.cell.n = 0
        cell.resetTheCell()
//        tableView.reloadData()
//        tableView.reloadRows(at: [indexPath], with: .fade)
        }
        let delete = UIContextualAction(style: .destructive, title: "Delete") { _, _, _ in
            self.numberOfRows.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade) }
        let swipeAction = UISwipeActionsConfiguration(actions: [delete, reset])
        return swipeAction
    }
    
}
/*  if (editingStyle == .insert) {
            
        }*/
    
    

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


